// EJEMPLO 4.1 LEER DATA POINTS DE ARCHIVO


int ejemplo41(){

    TCanvas *c1 = new TCanvas("c1","",1280,720);
    c1->SetGrid();

    TGraphErrors *gr = new TGraphErrors("ejemplo41data2.txt","%lg %lg %lg");
    gr->SetTitle("Medidas de x, y y error en x;""cm;""unidad arbitraria");
    gr->SetFillColor(kYellow);
    gr->DrawClone("E3AL"); // E3 dibuja la banda

    TGraphErrors *gr2 = new TGraphErrors("ejemplo41data.txt","%lg %lg %lg");
    gr2->SetMarkerStyle(kCircle);
    gr2->SetFillColor(0);
    gr2->DrawClone("PESame");

    // Leyenda
    TLegend leg(.1,.7,.3,.9,"LEYENDA EJEMPLO 4.1");
    leg.SetFillColor(0);
    leg.AddEntry(gr2,"Valor esperado");
    leg.AddEntry(gr,"Valor medido");
    leg.DrawClone("Same");

    gr->Print();
    return 0;
}
