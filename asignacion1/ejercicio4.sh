#!/bin/bash
#EJERCICIO 4#

ls -aRS > dirlog.log #para guardar en dirlog todos los directorios#


#Pedir y almacenar el nombre del directorio#
echo -n "Ingrese el directorio que busca: "
read n

#buscar con grep en el log#
grep -w  $n dirlog.log

#la shell comprueba si existe un directorio con ese nombre#
if [ -d $n ]; then
        echo El directorio existe 
else
        echo El directorio no existe
fi

#revisar que el directorio tenga permisos de escritura#
ls -l | grep $n | grep rw

if [ -w $n ]; then
	echo Se tiene permisos de escritura
else
	echo No se tiene permisos de escritura
fi


#Los brackets en if son una referencia del comando test: https://ryanstutorials.net/bash-scripting-tutorial/bash-if-statements.php#
