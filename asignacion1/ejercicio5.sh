#!/bin/bash
#EJERCICIO 5#
cd

cd Documents/

ls -lhR > log.txt

echo -n "Que palabra busca en el log: "
read n

#w es para que busque exactamente la palabra y c cuenta cuantas veces se repite#
#i en caso se quiera diferenciar entre mayus y minus#

echo "Esa palabra se repite: "
grep -wc $n log.txt

#sed se puede usar para modificar lineas de un fichero#
sed 's/aeotqf/ESTUDIANTE/g' log.txt


